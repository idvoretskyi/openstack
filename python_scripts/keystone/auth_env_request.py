#!/usr/bin/env python2
import requests
import os
import pprint

auth_url = os.environ['OS_AUTH_URL']
tenantName = os.environ['OS_TENANT_NAME']
username = os.environ['OS_USERNAME']
password = os.environ['OS_PASSWORD']

pp = pprint.PrettyPrinter(indent=4)

r = requests.post(
    auth_url,
    params={
        "auth": {
            "tenantName": tenantName,
            "passwordCredentials": {
                "username": username,
                "password": password
            }
        }
    }
)

print "'" + auth_url + "'"
print "'" + tenantName + "'"
print "'" + username + "'"
print "'" + password + "'"
