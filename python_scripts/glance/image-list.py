#!/usr/bin/env python2
import os
import pprint

from glanceclient.v2 import client
from keystoneclient.auth.identity import v2
from keystoneclient import session

pp = pprint.PrettyPrinter(indent=4)
'''
Authentication
'''
auth = v2.Password(auth_url=os.environ['OS_AUTH_URL'],
                   username=os.environ['OS_USERNAME'],
                   password=os.environ['OS_PASSWORD'],
                   tenant_id=os.environ['OS_TENANT_ID'])

sess = session.Session(auth=auth)

# Glance
glance = client.Client(sess)

def images_list():
    images = glance.images.list()
    return images
print images_list()
