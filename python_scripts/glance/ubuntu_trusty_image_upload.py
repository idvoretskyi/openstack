#!/usr/bin/env python2
import requests
import json
from sys import argv

script, auth_api_url, image_api_url, tenantName, username, password = argv

# auth_api_url = os.environ['OS_AUTH_URL']
# image_api_url = "http://172.16.19.2:9292"
# tenantName = os.environ['OS_TENANT_NAME']
# username = os.environ['OS_USERNAME']
# password = os.environ['OS_PASSWORD']

# print script
print auth_api_url
print image_api_url
print tenantName
print username
print password

# Get keystone token
url_auth = auth_api_url + '/tokens'
data_auth = {"auth": {
    "tenantName": tenantName,
    "passwordCredentials": {
        "username": username,
        "password": password
    }
}
}
headers_auth = {'Content-Type': 'application/json'}

r_auth = requests.post(
    url_auth,
    data=json.dumps(data_auth),
    headers=headers_auth)
resp = r_auth.json()['access']['token']

token = resp['id']
t_id = resp['tenant']['id']
t_name = resp['tenant']['name']

# List default images
url_list = image_api_url + '/v1/images/detail'
headers = {
    'X-Auth-Project-Id': t_name,
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'X-Auth-Token': token
}

r_list = requests.get(url_list, headers=headers)
resp = r_list.json()['images']
image_id = resp[0]['id']

# Delete existing images
resp = r_list.json()['images']

for response in resp:
    image_id = response['id']
    url_delete = image_api_url + '/v1/images/' + image_id
    r_del = requests.delete(url_delete, headers=headers)

# ADD UBUNTU IMAGE
url_add = image_api_url + '/v1/images'
headers_add = {
    'Content-Type': 'application/json',
    'x-image-meta-container_format': 'bare',
    'x-image-meta-location': 'https://cloud-images.ubuntu.com/trusty/current/trusty-server-cloudimg-amd64-disk1.img',
    'x-image-meta-is_public': 'False',
    'X-Auth-Token': token,
    'x-image-meta-disk_format': 'qcow2',
    'x-image-meta-name': 'Ubuntu 14.04 x64'}

image_add = requests.post(url_add, headers=headers_add)
print image_add
