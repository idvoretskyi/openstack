#!/usr/bin/env python2
import os
import pprint

from novaclient.v2 import client as nova
from keystoneclient.auth.identity import v2
from keystoneclient import session

pp = pprint.PrettyPrinter(indent=4)
'''
Authentication
'''
auth = v2.Password(auth_url=os.environ['OS_AUTH_URL'],
                   username=os.environ['OS_USERNAME'],
                   password=os.environ['OS_PASSWORD'],
                   tenant_id=os.environ['OS_TENANT_ID'])

sess = session.Session(auth=auth)

homedir = os.environ['HOME']
# noinspection PyArgumentList,PyTypeChecker
nova = nova.Client(session=sess)

def keypair_create():
    f = open('%s/.ssh/id_rsa.pub' % homedir, 'r')
    publickey = f.readline()[:-1]
    keypair = nova.keypairs.create('id_rsa', publickey)
    f.close()
    return keypair

print pp.pprint(keypair_create())

def keypairs_list():
    keypairs = nova.keypairs.list()
    return keypairs

print pp.pprint(keypairs_list())


def flavors_list():
    flavors = nova.flavors.list()
    return pp.pprint(flavors)


print flavors_list()


def images_list():
    images = nova.images.list()
    return pp.pprint(images)

print images_list()


def list_instances():
    instances = nova.servers.list()
    return pp.pprint(instances)


print list_instances()


# def launch_instance():

