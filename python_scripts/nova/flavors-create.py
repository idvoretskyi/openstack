import requests
import json
import pprint

pp = pprint.PrettyPrinter(indent=4)

auth_api_url = "http://172.16.19.2:5000/v2.0"
nova_api_url = "http://172.16.19.2:8774/v2/1fda99af1e2e4625ade3c0b16e0dd4a6"
tenantName = "admin"
username = "admin"
password = "admin"

flavors = [{ "name": "i1-standard-1-2", "ram": "1024", "vcpus": "1", "disk": "40", "id": "1" },
            { "name": "i1-standard-1-4", "ram": "4096", "vcpus": "1", "disk": "40", "id": "2" },
            { "name": "i1-standard-2-8", "ram": "8192", "vcpus": "2", "disk": "40", "id": "3" },
            { "name": "i1-standard-4-16", "ram": "16384", "vcpus": "4", "disk": "40", "id": "4" },
            { "name": "i1-standard-8-32", "ram": "32768", "vcpus": "8", "disk": "40", "id": "5" }]

##############KEYSTONE_GET_TOKEN#######################

url_auth = auth_api_url+'/tokens'
data_auth = {"auth": {
              "tenantName": tenantName,
              "passwordCredentials": {
                "username": username,
                "password": password
                }
              }
            }
headers_auth = {'Content-Type': 'application/json'}
print data_auth

r_auth = requests.post(url_auth, data=json.dumps(data_auth), headers=headers_auth)
resp = r_auth.json()['access']['token']
print resp

token    = resp['id']
t_id     = resp['tenant']['id']
t_name   = resp['tenant']['name']

#######GET DEFAULT FLAVOR IDS##############

url_list = nova_api_url+'/v2/'+t_id+'/flavors'
headers={
    'X-Auth-Project-Id': t_name,
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'X-Auth-Token' : token
    }

r_list = requests.get(url_list,headers=headers)
resp = r_list.json()['flavors']
flavors_id=resp[0]['id']
print flavors_id

####### DELETE FLAVORS #########

resp = r_list.json()['flavors']

for response in resp:
       image_id=response['id']
       url_delete = nova_api_url+'/v2/'+t_id+'/flavors/'+flavors_id
       r_del = requests.delete(url_delete, headers=headers)

######ADD FLAVORS############

url_add = nova_api_url+'/v2/'+t_id+'/flavors'

def flavor_add(name, ram, vcpus, disk, id):
    data_add = {'flavor':{
        'name': name,
        'ram': ram,
        'vcpus': vcpus,
        'disk': disk,
        'id': id,
        }
    }
    f_add = requests.post(url_add, data=json.dumps(data_add), headers=headers)
for flavor in flavors:
    flavor_add(flavor['name'], flavor['ram'], flavor['vcpus'], flavor['disk'], flavor['id'])
