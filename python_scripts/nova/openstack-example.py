#!/usr/bin/env python2
import os
import pprint

from novaclient.v2 import client
from keystoneclient.auth.identity import v2
from keystoneclient import session

'''
Authentication
'''
auth = v2.Password(auth_url=os.environ['OS_AUTH_URL'],
                   username=os.environ['OS_USERNAME'],
                   password=os.environ['OS_PASSWORD'],
                   tenant_id=os.environ['OS_TENANT_ID'])

sess = session.Session(auth=auth)

print sess
pp = pprint.PrettyPrinter(indent=4)
# noinspection PyArgumentList,PyTypeChecker
nova = client.Client(session=sess)


def keypairs_list():
    keypairs = nova.keypairs.list()
    return pp.pprint(keypairs)


print keypairs_list()


def flavors_list():
    flavors = nova.flavors.list()
    return pp.pprint(flavors)


print flavors_list()


def list_instances():
    instances = nova.servers.list()
    return pp.pprint(instances)


print list_instances()
