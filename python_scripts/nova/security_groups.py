import requests
import json
import pprint

pp = pprint.PrettyPrinter(indent=4)

auth_api_url = "http://192.168.110.1:5000/v2.0"
nova_api_url = "http://192.168.110.1:8774"

tenantName = "test_env"
username = "test_env"
password = "SoBee4eTSoBee4eT"
new_rules = [{'from_port': 80, 'to_port': 80, 'protocol': 'tcp', 'cidr': '0.0.0.0/0'},
        {'from_port': 443, 'to_port': 443, 'protocol': 'tcp', 'cidr': '0.0.0.0/0'},
        {'from_port': 1, 'to_port': 65535, 'protocol': 'tcp', 'cidr': '192.168.0.0/16'},
        {'from_port': -1, 'to_port': -1, 'protocol': 'icmp', 'cidr': '192.168.0.0/16'},
        {'from_port': 1, 'to_port': 65535, 'protocol': 'udp', 'cidr': '192.168.0.0/16'},
        {'from_port': 1, 'to_port': 65535, 'protocol': 'tcp', 'cidr': '185.39.228.0/24'},
        {'from_port': 1, 'to_port': 65535, 'protocol': 'udp', 'cidr': '185.39.228.0/24'},
        {'from_port': 1, 'to_port': 65535, 'protocol': 'tcp', 'cidr': '10.10.0.0/16'},
        {'from_port': 1, 'to_port': 65535, 'protocol': 'udp', 'cidr': '10.10.0.0/16'}]

##############KEYSTONE_GET_TOKEN#######################

url_auth = auth_api_url+'/tokens'
data_auth = {"auth": {
              "tenantName": tenantName,
              "passwordCredentials": {
                "username": username,
                "password": password
                }
              }
            }
headers_auth = {'Content-Type': 'application/json'}

r_auth = requests.post(url_auth, data=json.dumps(data_auth), headers=headers_auth)
resp = r_auth.json()['access']['token']

token	= resp['id']
t_id	= resp['tenant']['id']
t_name	= resp['tenant']['name']

################ NOVA SEC GROUP LIST AND ADD RULE ###################

####### GET DEFAULT SEC GROUP ID ##############

url_list = nova_api_url+'/v2/'+t_id+'/os-security-groups'
headers={
    'X-Auth-Project-Id': t_name,
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'X-Auth-Token' : token
    }

r_list = requests.get(url_list,headers=headers)
resp = r_list.json()['security_groups']
sec_group_id=resp[0]['id']

####### DELETE DEFAULT SECURITY RULES #########

resp = r_list.json()['security_groups']
sec_group_id=resp[0]['id']
rules=resp[0]['rules']

for id in rules:
       url_delete = nova_api_url+'/v2/'+t_id+'/os-security-group-rules/'+str(id['id'])
       pp.pprint(url_delete)
       r_del=requests.delete(url_delete, headers=headers)

######ADD RULES TO SECURITY GROUP############

url_add = nova_api_url+'/v2/'+t_id+'/os-security-group-rules'

def sec_rule(from_port, to_port, ip_protocol, cidr):
    data_add = {'security_group_rule':{
        'from_port': from_port,
        'to_port': to_port,
        'ip_protocol': ip_protocol,
        'parent_group_id': sec_group_id,
        'cidr': cidr,
        }
    }

    r_add = requests.post(url_add, data=json.dumps(data_add), headers=headers)
    pp.pprint(r_add)

for rule in new_rules:
    sec_rule(rule['from_port'], rule['to_port'], rule['protocol'], rule['cidr'])
